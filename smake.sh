#!/bin/bash
if [ -f "smake.functions" ]
then
	. smake.functions
else
	. /usr/lib/smake/smake.functions
fi
arg_parse $*
[ -d "$WORKDIR" ] && cd "$(readlink -f $WORKDIR)"
if [ -f "$SMAKE_FILE" ]
then
	. $SMAKE_FILE
else
	. smake.build
fi
[ "$DISABLE_CHECK" != "true" ] && check
get_defaults
arg_parse $*
for i in $*
do
	if echo $i | grep -v "=" > /dev/null
	then
		export SMAKE_PROCESS=$i
		info "Starting" "$SMAKE_PROCESS\n"
		[ -d "$WORKDIR" ] && cd "$(readlink -f $WORKDIR)"
		unset $i
		set -u
		$NAME-$SMAKE_PROCESS
		set +u
		info "Done" "$SMAKE_PROCESS\n"
	fi
done
